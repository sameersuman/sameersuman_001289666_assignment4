/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 *
 *
 */
public class Student {

    private String studentID;
    private String salary;
    private String GPA;
    private String gradYear;
    College c = new College();
    Department d = new Department();
    Course co = new Course();
    University u = new University();
    Applicants a = new Applicants();

    public String getStudentID() {
        return studentID;
    }

    public void setStudentID(String studentID) {
        this.studentID = studentID;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getGPA() {
        return GPA;
    }

    public void setGPA(String GPA) {
        this.GPA = GPA;
    }

    public void get_other_value() {
        System.out.println("college name  = " + c.getCollegName());
        System.out.println("course name  = " + co.getCourseName());
        System.out.println("professor name  = " + co.getProfName());
        System.out.println("univ name  = " + u.getUniversityName() + " " + u.getRegion());
        System.out.println("dept name  = " + d.getDepartmentName());
        System.out.println("Applicant Number  = " + a.getApplicantNumber());
        System.out.println("Seats   = " + co.getSeats());
    }

    public String get_dept_name() {
        return d.getDepartmentName();
    }

    public String get_coll_name() {
        return c.getCollegName();
    }

    public String get_course_name() {
        return co.getCourseName();
    }

    public String get_app_number() {
        return a.getApplicantNumber();
    }

    public String get_seats() {
        return co.getSeats();
    }

    public String get_uni_name() {
        return u.getUniversityName();
    }

    public String getGradYear() {
        return gradYear;
    }

    public void setGradYear(String gradYear) {
        this.gradYear = gradYear;
    }

    //List<String> deck = new ArrayList<String>();
    Student(String uname, String reg, String cr_name, String co_name, String prof_name, String dept, String gpa, String gyear, String studentID, String salary, String app_number, String seats) {

        u.setUniversityName(uname);
        u.setRegion(reg);
        c.setCollegName(co_name);
        co.setCourseName(cr_name);
        co.setProfName(prof_name);
        d.setDepartmentName(dept);
        a.setApplicantNumber(app_number);
        co.setSeats(seats);
        this.setGPA(gpa);
        this.setGradYear(gyear);
        this.setStudentID(studentID);
        this.setSalary(salary);
    }

    Student(String app_number, String seats) {
        //a.setApplicantNumber(app_number);
        // co.setSeats(seats);
//       co.setCourseName(course);
//       c.setCollegName(college);
//       d.setDepartmentName(department);
//       u.setUniversityName(university);

    }

    public static void main(String args[]) {

//        Student s1 = new Student("NEU", "boston", "DB", "COE", "richard", "IS", "3.7", "2010", "sameer", "50000");
//        Student s2 = new Student("NEU", "boston", "DB", "COE", "richard", "IS", "3.7", "2010", "raju", "50000");
//        Student s3 = new Student("NEU", "boston", "DB", "COE", "richard", "IS", "3.7", "2010", "adi", "50000");
        // s.get_other_value();
//        System.out.println(s.getGPA());
//        System.out.println(s.getName());
        String fileName = "Output.txt";
        ArrayList<Student> studentlist = new ArrayList<Student>();
//        studentlist.add(s1);
//        studentlist.add(s2);
//        studentlist.add(s3);
        try {
            PrintWriter out = new PrintWriter(fileName);

            for (int j = 0; j < 3000; j++) {
                studentlist.add(new Student("University" + (j % 5), "region" + (j % 5), "course" + (j % 10), "college" + (j % 4), "professor" + (j % 20), "department" + (j % 10), "3." + (j % 10), "201" + (j % 7), "University ID" + j, "" + (j % 50000), "2" + (j % 10), "20" + (j % 4)));
            }

            // studentlist.get(j).get_other_value();
//        System.out.println(studentlist.get(m).getGPA());
//        System.out.println("StudentID  "+studentlist.get(m).getStudentID());
//        System.out.println("Salary  " +studentlist.get(m).getSalary());
//        System.out.println("Year "+studentlist.get(m).getGradYear());
//        System.out.println("----------------------------------------------------");
//        
//        }
            out.println("**********************************************************************************************");
            out.println("--------------------------Average GPA Report by Course------------------------------------");
            out.println("**********************************************************************************************");
            float p, l;

            p = 0;
            l = 0;
            String course;

            for (int t1 = 0; t1 < 10; t1++) {
                course = studentlist.get(t1).get_course_name();
                for (int t = 0; t < studentlist.size(); t++) {
                    if (studentlist.get(t).get_course_name().equals(course)) {

                        p += Float.parseFloat((studentlist.get(t).getGPA()));
                        l++;
                    }
                }
                out.println("avg gpa by course = " + p / l + " for course  = " + course);
            }
            out.println("**********************************************************************************************");
            out.println("--------------------------Average Salary Report by Course------------------------------------");
            out.println("**********************************************************************************************");
            float psal, lsal;

            psal = 0;
            lsal = 0;
            String coursesal;

            for (int t21 = 0; t21 < 10; t21++) {
                coursesal = studentlist.get(t21).get_course_name();
                for (int t22 = 0; t22 < studentlist.size(); t22++) {
                    if (studentlist.get(t22).get_course_name().equals(coursesal)) {
                        psal += Float.parseFloat((studentlist.get(t22).getSalary()));
                        lsal++;
                    }
                }
                out.println("avg salary by course = " + psal / lsal + " for course  = " + coursesal);
            }
            out.println("**********************************************************************************************");
            out.println("--------------------------Average Demand Report by Course------------------------------------");
            out.println("**********************************************************************************************");
            float tdem, ldem;

            tdem = 0;
            ldem = 0;
            String coursedem;

            for (int t51 = 0; t51 < 10; t51++) {
                coursedem = studentlist.get(t51).get_course_name();
                for (int t52 = 0; t52 < studentlist.size(); t52++) {
                    if (studentlist.get(t52).get_course_name().equals(coursedem)) {
                        tdem += Float.parseFloat((studentlist.get(t52).get_app_number()));
                        ldem++;
                    }
                }
                out.println("Percentage of successful admits = " + (ldem / tdem) * 100 + "%" + " for course  = " + coursedem);
            }
            out.println("**********************************************************************************************");
            out.println("--------------------------Average GPA Report by Department------------------------------------");
            out.println("**********************************************************************************************");
            float temp, k;

            temp = 0;
            k = 0;
            String dept;
            String courses;
            for (int t3 = 0; t3 < 10; t3++) {
                dept = studentlist.get(t3).get_dept_name();
                for (int t1 = 0; t1 < 10; t1++) {
                    courses = studentlist.get(t1).get_course_name();
                    for (int t = 0; t < studentlist.size(); t++) {
                        if (studentlist.get(t).get_dept_name().equals(dept)) {

                            if (studentlist.get(t).get_course_name().equals(courses)) {
                                temp += Float.parseFloat((studentlist.get(t).getGPA()));
                                k++;
                            }
                        }
                    }
                    out.println("avg gpa by department & course  = " + temp / k + "  for course  = " + courses + "  for department  = " + dept);
                }
            }

            out.println("**********************************************************************************************");
            out.println("--------------------------Average Salary Report by Department------------------------------------");
            out.println("**********************************************************************************************");
            float tempsal, ksal;

            tempsal = 0;
            ksal = 0;
            String deptsal;
            String coursessal;
            for (int t30 = 0; t30 < 10; t30++) {
                deptsal = studentlist.get(t30).get_dept_name();
                for (int t31 = 0; t31 < 10; t31++) {
                    coursesal = studentlist.get(t31).get_course_name();
                    for (int t33 = 0; t33 < studentlist.size(); t33++) {
                        if (studentlist.get(t33).get_dept_name().equals(deptsal));
                        {

                            if (studentlist.get(t33).get_course_name().equals(coursesal)) {
                                tempsal += Float.parseFloat((studentlist.get(t33).getSalary()));
                                ksal++;
                            }
                        }
                    }
                    out.println("avg salary by department & course  = " + tempsal / ksal + "  for course  = " + coursesal + "  for department  = " + deptsal);
                }
            }

            out.println("**********************************************************************************************");
            out.println("--------------------------Average demand Report by Department------------------------------------");
            out.println("**********************************************************************************************");
            float tempdem, kdem;

            tempdem = 0;
            kdem = 0;
            String deptdem;
            String coursesdem;
            for (int t63 = 0; t63 < 10; t63++) {
                deptdem = studentlist.get(t63).get_dept_name();
                for (int t64 = 0; t64 < 10; t64++) {
                    coursesdem = studentlist.get(t64).get_course_name();
                    for (int t65 = 0; t65 < studentlist.size(); t65++) {
                        if (studentlist.get(t65).get_dept_name().equals(deptdem)) {

                            if (studentlist.get(t65).get_course_name().equals(coursesdem)) {
                                tempdem += Float.parseFloat((studentlist.get(t65).get_app_number()));
                                kdem++;
                            }
                        }
                    }
                    out.println("Percentage of successful admits  = " + (kdem / tempdem) * 100 + "%" + "  for course  = " + coursesdem + "  for department  = " + deptdem);
                }
            }

            out.println("**********************************************************************************************");
            out.println("--------------------------Average GPA Report by College------------------------------------");
            out.println("**********************************************************************************************");
            float temtp, z;

            temtp = 0;
            z = 0;
            String deptz;
            String coursesm;
            String colName;

            for (int t13 = 0; t13 < 4; t13++) {
                colName = studentlist.get(t13).get_coll_name();

                for (int t15 = 0; t15 < 10; t15++) {
                    deptz = studentlist.get(t15).get_dept_name();
                    for (int t11 = 0; t11 < 10; t11++) {
                        coursesm = studentlist.get(t11).get_course_name();
                        for (int t12 = 0; t12 < studentlist.size(); t12++) {
                            if (studentlist.get(t12).get_coll_name().equals(colName)) {
                                if (studentlist.get(t12).get_dept_name().equals(deptz)) {

                                    if (studentlist.get(t12).get_course_name().equals(coursesm)) {
                                        temtp += Float.parseFloat((studentlist.get(t12).getGPA()));
                                        z++;
                                    }
                                }
                            }
                        }
                        out.println("avg gpa by dept & course & college  = " + temtp / z + "  for course  = " + coursesm + "  for depatrment   " + deptz + "   for college  " + colName);
                    }
                }
            }

            out.println("**********************************************************************************************");
            out.println("--------------------------Average Salary Report by College------------------------------------");
            out.println("**********************************************************************************************");
            float temtpsal, zsal;

            temtpsal = 0;
            zsal = 0;
            String deptzsal;
            String coursesmsal;
            String colNamesal;

            for (int t34 = 0; t34 < 4; t34++) {
                colNamesal = studentlist.get(t34).get_coll_name();

                for (int t35 = 0; t35 < 10; t35++) {
                    deptzsal = studentlist.get(t35).get_dept_name();
                    for (int t36 = 0; t36 < 10; t36++) {
                        coursesmsal = studentlist.get(t36).get_course_name();
                        for (int t37 = 0; t37 < studentlist.size(); t37++) {
                            if (studentlist.get(t37).get_coll_name().equals(colNamesal)) {
                                if (studentlist.get(t37).get_dept_name().equals(deptzsal)) {

                                    if (studentlist.get(t37).get_course_name().equals(coursesmsal)) {
                                        temtpsal += Float.parseFloat((studentlist.get(t37).getSalary()));
                                        zsal++;
                                    }
                                }
                            }
                        }
                        out.println("avg salary by dept & course & college  = " + temtpsal / zsal + "  for course  = " + coursesmsal + "  for depatrment   " + deptzsal + "   for college  " + colNamesal);
                    }
                }
            }

            out.println("**********************************************************************************************");
            out.println("--------------------------Average Demand Report by College------------------------------------");
            out.println("**********************************************************************************************");
            float temtpdem, zdem;

            temtpdem = 0;
            zdem = 0;
            String deptzdem;
            String coursesmdem;
            String colNamedem;

            for (int t67 = 0; t67 < 4; t67++) {
                colNamedem = studentlist.get(t67).get_coll_name();

                for (int t68 = 0; t68 < 10; t68++) {
                    deptzdem = studentlist.get(t68).get_dept_name();
                    for (int t69 = 0; t69 < 10; t69++) {
                        coursesmdem = studentlist.get(t69).get_course_name();
                        for (int t70 = 0; t70 < studentlist.size(); t70++) {
                            if (studentlist.get(t70).get_coll_name().equals(colNamedem)) {
                                if (studentlist.get(t70).get_dept_name().equals(deptzdem)) {

                                    if (studentlist.get(t70).get_course_name().equals(coursesmdem)) {
                                        temtpdem += Float.parseFloat((studentlist.get(t70).get_app_number()));
                                        zdem++;
                                    }
                                }
                            }
                        }
                        out.println("Percentage of successful admits  = " + (zdem / temtpdem) * 100 + "%" + "  for course  = " + coursesmdem + "  for depatrment   " + deptzdem + "   for college  " + colNamedem);
                    }
                }
            }

            out.println("**********************************************************************************************");
            out.println("--------------------------Average GPA Report by University------------------------------------");
            out.println("**********************************************************************************************");
            float tempu, u;

            tempu = 0;
            u = 0;
            String deptu;
            String courseu;
            String colNameu;
            String univ;

            for (int t16 = 0; t16 < 5; t16++) {
                univ = studentlist.get(t16).get_uni_name();

                for (int t17 = 0; t17 < 4; t17++) {
                    colNameu = studentlist.get(t17).get_coll_name();

                    for (int t18 = 0; t18 < 10; t18++) {
                        deptu = studentlist.get(t18).get_dept_name();

                        for (int t19 = 0; t19 < 10; t19++) {
                            courseu = studentlist.get(t19).get_course_name();
                            for (int t20 = 0; t20 < studentlist.size(); t20++) {
                                if (studentlist.get(t20).get_uni_name().equals(univ)) {
                                    if (studentlist.get(t20).get_coll_name().equals(colNameu)) {
                                        if (studentlist.get(t20).get_dept_name().equals(deptu)) {

                                            if (studentlist.get(t20).get_course_name().equals(courseu)) {
                                                tempu += Float.parseFloat((studentlist.get(t20).getGPA()));
                                                u++;
                                            }
                                        }
                                    }
                                }
                            }

                            out.println("avg gpa by dept & course & college & university  = " + tempu / u + "  for course  = " + courseu + "  for depatrment   " + deptu + "   for college  " + colNameu + "  for university   " + univ);
                        }
                    }
                }
            }

            out.println("**********************************************************************************************");
            out.println("--------------------------Average Salary Report by University------------------------------------");
            out.println("**********************************************************************************************");
            float tempusal, usal;

            tempusal = 0;
            usal = 0;
            String deptusal;
            String courseusal;
            String colNameusal;
            String univsal;

            for (int t46 = 0; t46 < 5; t46++) {
                univsal = studentlist.get(t46).get_uni_name();

                for (int t47 = 0; t47 < 4; t47++) {
                    colNameusal = studentlist.get(t47).get_coll_name();

                    for (int t48 = 0; t48 < 10; t48++) {
                        deptusal = studentlist.get(t48).get_dept_name();

                        for (int t49 = 0; t49 < 10; t49++) {
                            courseusal = studentlist.get(t49).get_course_name();
                            for (int t45 = 0; t45 < studentlist.size(); t45++) {
                                if (studentlist.get(t45).get_uni_name().equals(univsal)) {
                                    if (studentlist.get(t45).get_coll_name().equals(colNameusal)) {
                                        if (studentlist.get(t45).get_dept_name().equals(deptusal)) {

                                            if (studentlist.get(t45).get_course_name().equals(courseusal)) {
                                                tempusal += Float.parseFloat((studentlist.get(t45).getSalary()));
                                                usal++;
                                            }
                                        }
                                    }
                                }
                            }

                            out.println("avg salary by dept & course & college & university  = " + tempusal / usal + "  for course  = " + courseusal + "  for depatrment   " + deptusal + "   for college  " + colNameusal + "  for university   " + univsal);
                        }
                    }
                }
            }

            out.println("**********************************************************************************************");
            out.println("--------------------------Average demand Report by University------------------------------------");
            out.println("**********************************************************************************************");
            float tempudem, udem;

            tempudem = 0;
            udem = 0;
            String deptudem;
            String courseudem;
            String colNameudem;
            String univdem;

            for (int t71 = 0; t71 < 5; t71++) {
                univdem = studentlist.get(t71).get_uni_name();

                for (int t72 = 0; t72 < 4; t72++) {
                    colNameudem = studentlist.get(t72).get_coll_name();

                    for (int t73 = 0; t73 < 10; t73++) {
                        deptudem = studentlist.get(t73).get_dept_name();

                        for (int t74 = 0; t74 < 10; t74++) {
                            courseudem = studentlist.get(t74).get_course_name();
                            for (int t75 = 0; t75 < studentlist.size(); t75++) {
                                if (studentlist.get(t75).get_uni_name().equals(univdem)) {
                                    if (studentlist.get(t75).get_coll_name().equals(colNameudem)) {
                                        if (studentlist.get(t75).get_dept_name().equals(deptudem)) {

                                            if (studentlist.get(t75).get_course_name().equals(courseudem)) {
                                                tempudem += Float.parseFloat((studentlist.get(t75).get_app_number()));
                                                udem++;
                                            }
                                        }
                                    }
                                }
                            }

                            out.println("Percentage of successful admits  = " + (udem / tempudem) * 100 + "%" + "  for course  = " + courseudem + "  for depatrment   " + deptudem + "   for college  " + colNameudem + "  for university   " + univdem);
                        }
                    }
                }
            }

            // System.out.println("--------------------------Report by College------------------------------------");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
